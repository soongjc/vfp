package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// SmartContract provides functions to manage invoices
type SmartContract struct {
	contractapi.Contract
}

// Invoice describes basic details of a real world invoice
type Invoice struct {
	Issuer        string `json:"issuer"`
	InvoiceNumber string `json:"invoiceNumber"`
	Owner         string `json:"owner"`
	IssueDate     string `json:"issueDate"`
	DueDate       string `json:"dueDate"`
	State         string `json:"State"`
}

// InitLedger adds a base set of cars to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {

	invoices := []Invoice{
		Invoice{Issuer: "vendor1", InvoiceNumber: "v1_12345", Owner: "vendor1", IssueDate: "01-JAN-2020", DueDate: "01-FEB-2020", State: "issue"},
		Invoice{Issuer: "vendor2", InvoiceNumber: "v2_11111", Owner: "hlb", IssueDate: "01-JAN-2020", DueDate: "01-FEB-2020", State: "trading"},
	}

	for i, invoice := range invoices {
		invoiceAsBytes, _ := json.Marshal(invoice)
		err := ctx.GetStub().PutState("INV"+strconv.Itoa(i), invoiceAsBytes)

		if err != nil {
			return fmt.Errorf("Failed to put to world state. %s", err.Error())
		}
	}

	return nil
}

// QueryInvoice returns the invoice stored in the world state with given ref id
func (s *SmartContract) QueryInvoice(ctx contractapi.TransactionContextInterface, invoiceRef string) (*Invoice, error) {
	invoiceAsBytes, err := ctx.GetStub().GetState(invoiceRef)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if invoiceAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", invoiceRef)
	}

	invoice := new(Invoice)
	_ = json.Unmarshal(invoiceAsBytes, invoice)

	return invoice, nil
}

func main() {

	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		fmt.Printf("Error create fabcar chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting fabcar chaincode: %s", err.Error())
	}

}
