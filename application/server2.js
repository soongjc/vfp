//var wallet = require('./wallet.js')

// wallet.add("User1").then(() => {
//     console.log("done");
// }).catch((e) => {
//     console.log(e);
//     console.log(e.stack);
//     process.exit(-1);
// });

'use strict';

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require('fs');
const { FileSystemWallet, Gateway } = require('fabric-network');
const yaml = require('js-yaml');
const user1Wallet = new FileSystemWallet('./wallet');
console.log(user1Wallet)
const gateway = new Gateway();

const userName = 'Admin@org1.example.com';

let connectionProfile = yaml.safeLoad(fs.readFileSync('./networkConnection.yaml', 'utf8'));
let connectionOptions = {
    identity: userName,
    wallet: user1Wallet,
    discovery: { enabled: false, asLocalhost: true }
};
console.log(connectionOptions)
// Connect to gateway using application specified parameters
console.log('Connect to Fabric gateway.');

gateway.connect(connectionProfile, connectionOptions).catch((e)=>{
    console.log(e);
})
console.log('Use network channel: mychannel.');

const network = gateway.getNetwork('mychannel').catch((e)=>{
    console.log(e)
});