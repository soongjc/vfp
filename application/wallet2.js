'use strict';

const fs = require('fs');
const { FileSystemWallet, X509WalletMixin } = require('fabric-network');
const path = require('path')

const fixtures = path.resolve(__dirname, '../../test-network');

module.exports = {

    add: async (name) => {
        try {
            // Identity to credentials to be stored in the wallet
            const wallet = new FileSystemWallet('./identity/user/'+name+'/wallet');
            const credPath = path.join(fixtures, '/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com');
            const cert = fs.readFileSync(path.join(credPath, '/msp/signcerts/cert.pem')).toString();
            const key = fs.readFileSync(path.join(credPath, '/msp/keystore/b8068f1a8c67f869e76690157775efe582b73e0e2b17680e16e7bcdfa5d8f05f_sk')).toString();
    
            // Load credentials into wallet
            const identityLabel = 'Admin@org1.example.com';
            const identity = X509WalletMixin.createIdentity('Org1MSP', cert, key);
    
            await wallet.import(identityLabel, identity);
    
        } catch (error) {
            console.log(`Error adding to wallet. ${error}`);
            console.log(error.stack);
        }
    }

};
