'use strict';

const { Gateway, Wallets } = require('fabric-network');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');
const gateway = new Gateway();

async function main() {

    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    const identity = await wallet.get('admin');
    if (!identity) {
        console.log('An identity for the user "admin" does not exist in the wallet');
        return;
    }

    const gateway = new Gateway();

    let connectionProfile = yaml.safeLoad(fs.readFileSync('./networkConnection.yaml', 'utf8'));

    let connectionOptions = {
        identity: 'admin',
        wallet: wallet,
        discovery: { enabled: true, asLocalhost: true }
    };
    // Connect to gateway using application specified parameters
    console.log('Connect to Fabric gateway.');

    await gateway.connect(connectionProfile, connectionOptions)
    console.log('Use network channel: mychannel.');
    const network = await gateway.getNetwork('mychannel');
    const contract = network.getContract('vfp');
    const result = await contract.evaluateTransaction('QueryInvoice','INV1');
    console.log(result.toString())

}

main().then(()=>{
    console.log('done');
})